import React, { useEffect } from 'react';
import { View, Alert } from 'react-native';
import { Provider } from "react-redux";
import { AsyncStorage } from 'react-native';
import messaging from "@react-native-firebase/messaging";
import store from "./src/redux/store/store";
import InitialisingScreen from "./src/screens/InitialisingScreen";
import { Notifications } from 'react-native-notifications';

export default class App extends React.Component {

  componentDidMount() {
    messaging()
      .getToken()
      .then(token => {
        console.log('token device now: ', token);
      });

    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        console.log('open notification');
      });
  }

  componentWillUnmount() {
    messaging().onMessage(async remoteMessage => {
      Notifications.postLocalNotification({
        title: remoteMessage.notification.title,
        body: remoteMessage.notification.body,
      });
    });
  }

  render() {
    return (
      <Provider store={store}>
        <InitialisingScreen />
      </Provider>
    )
  }

}