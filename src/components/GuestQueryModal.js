import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity, Modal } from "react-native";

export default class GuestQueryModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            adults: 1,
            children: 0,
            infants: 0,

            maxAdult: props.maxAdult ? props.maxAdult : 2,
            maxChildren: props.maxChildren ? props.maxChildren : 0,
            maxInfants: props.maxInfants ? props.maxInfants : 0
        }
    }
    togleModal = () => {
        this.setState({ modalVisible: !this.state.modalVisible });
    }

    increaseValue(type) {
        if (type == 'adults') {
            if (this.state.adults < this.state.maxAdult) {
                this.setState({ adults: this.state.adults + 1 });
            }
        } else if (type == 'children') {
            if (this.state.children < this.state.maxChildren) {
                this.setState({ children: (this.state.children + 1) });
            }
        } else {
            if (this.state.infants < this.state.maxInfants) {
                this.setState({ infants: (this.state.infants + 1) });
            }
        }
    }

    reduceValue(type) {
        if (type == 'adults') {
            this.state.adults > 1 ? this.setState({ adults: this.state.adults - 1 }) : null;
        } else if (type == 'children') {
            this.state.children > 0 ? this.setState({ children: (this.state.children - 1) }) : null;
        } else {
            this.state.infants > 0 ? this.setState({ infants: (this.state.infants - 1) }) : null;
        }
    }

    doneGuestQuery = () => {
        const guestQuery = {
            adults: this.state.adults,
            children: this.state.children,
            infants: this.state.infants
        }
        // console.log('guest query')
        this.props.getGuestQuery(guestQuery);
        this.togleModal();
    }
    render() {
        return (

            <Modal
                visible={this.state.modalVisible}
                transparent={true}
                animationType="slide"
                onRequestClose={this.togleModal}>
                <View style={styles.viewMain}>

                    <View style={styles.viewChildModal}>

                        <View style={styles.viewActionTouch}>

                            <TouchableOpacity style={styles.touchClose}
                                onPress={this.togleModal}>
                                <Text style={{ fontSize: 17 }}>Cancel</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.touchClose}
                                onPress={() => this.doneGuestQuery()}>
                                <Text style={{ fontSize: 17, color: '#3389EE' }}>Done</Text>
                            </TouchableOpacity>

                        </View>

                        {/* view body select number of people */}
                        <View style={styles.viewBodySelect}>

                            {/* Row: Adults */}
                            <View style={styles.viewRowSelect}>

                                {/* Title, age */}
                                <View style={styles.viewTitleSelect}>
                                    <Text>Adults</Text>
                                    <Text>16+ years</Text>
                                </View>

                                {/* Button increase or reduce */}
                                <View style={styles.viewButtonsIncrRedu}>

                                    <TouchableOpacity style={styles.touchIncrRedu} onPress={() => this.reduceValue('adults')}>
                                        <Text style={styles.markIncrRedu}>-</Text>
                                    </TouchableOpacity>

                                    <Text style={styles.textNumberIncrRedu}>
                                        {this.state.adults}
                                    </Text>

                                    <TouchableOpacity style={styles.touchIncrRedu} onPress={() => this.increaseValue('adults')}>
                                        <Text style={styles.markIncrRedu}>+</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            {/* Row: Children */}
                            <View style={styles.viewRowSelect}>

                                {/* Title, age */}
                                <View style={styles.viewTitleSelect}>
                                    <Text>Children</Text>
                                    <Text>2 - 17 years</Text>
                                </View>

                                {/* Button increase or reduce */}
                                <View style={styles.viewButtonsIncrRedu}>

                                    <TouchableOpacity style={styles.touchIncrRedu} onPress={() => this.reduceValue('children')}>
                                        <Text style={styles.markIncrRedu}>-</Text>
                                    </TouchableOpacity>

                                    <Text style={styles.textNumberIncrRedu}>
                                        {this.state.children}
                                    </Text>

                                    <TouchableOpacity style={styles.touchIncrRedu} onPress={() => this.increaseValue('children')}>
                                        <Text style={styles.markIncrRedu}>+</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            {/* Row: Infants */}
                            <View style={styles.viewRowSelect}>

                                {/* Title, age */}
                                <View style={styles.viewTitleSelect}>
                                    <Text>Infants</Text>
                                    <Text>under 2 years</Text>
                                </View>

                                {/* Button increase or reduce */}
                                <View style={styles.viewButtonsIncrRedu}>

                                    <TouchableOpacity style={styles.touchIncrRedu} onPress={() => this.reduceValue('infants')}>
                                        <Text style={styles.markIncrRedu}>-</Text>
                                    </TouchableOpacity>

                                    <Text style={styles.textNumberIncrRedu}>
                                        {this.state.infants}
                                    </Text>

                                    <TouchableOpacity style={styles.touchIncrRedu} onPress={() => this.increaseValue('infants')}>
                                        <Text style={styles.markIncrRedu}>+</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                        </View>
                    </View>
                </View>
            </Modal >

        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        flexWrap: "nowrap",
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(0,0,0,.65)',
    },
    viewChildModal: {
        backgroundColor: '#fff',
        paddingBottom: 10,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15
    },
    viewActionTouch: {
        // backgroundColor: '#383bff',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: "#E0E0E1",
    },
    toucClose: {
        flex: 1,
        justifyContent: 'center',
    },

    viewBodySelect: {
        paddingHorizontal: 15,
    },
    viewRowSelect: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: "#E0E0E1",
        // backgroundColor: 'grey',
    },
    viewTitleSelect: {
        // backgroundColor: 'orange'
    },
    viewButtonsIncrRedu: {
        width: 75,
        // backgroundColor: 'orange',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    touchIncrRedu: {
        backgroundColor: '#3389EE',
        width: 25,
        height: 25,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25
    },
    markIncrRedu: {
        fontSize: 25,
        color: 'white',
    },
    textNumberIncrRedu: {
        fontSize: 25,
        marginHorizontal: 10,
    }
});