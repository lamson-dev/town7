import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { Icon } from "react-native-elements";

export default class CardBookingRoom extends Component {

    renderStar = (numOfStar) => {
        let star = [];

        for (let index = 0; index < 5; index++) {

            if (index < numOfStar) {
                star.push(<Text key={index} style={styles.textStar}>★</Text>);
            } else {
                star.push(<Text key={index} style={styles.textStar}>☆</Text>);
            }
        };
        return star;
    }

    render() {
        const { hostName, address, imgUri, price, starVoting, startDate, endDate, numOfGuest, bookingItem, onPress } = this.props;
        let bookingStatus;
        if (!bookingItem.cancel) {

            if (bookingItem.acceptedFromHost) {
                if (bookingItem.paid) {
                    if (bookingItem.status) {
                        bookingStatus = <Text style={[styles.textSecondColor, styles.textSmall, { color: 'green' }]}> COMPLETED </Text>
                    } else {
                        bookingStatus = <Text style={[styles.textSecondColor, styles.textSmall, { color: 'green' }]}> Paid </Text>
                    }
                } else
                    bookingStatus = <Text style={[styles.textSecondColor, styles.textSmall, { color: '#adfc03' }]}> Waiting your payment </Text>

            } else {
                bookingStatus = <Text style={[styles.textSecondColor, styles.textSmall, { color: 'orange' }]}> Waiting host confirm </Text>
            }
        } else {
            bookingStatus = <Text style={[styles.textSecondColor, styles.textSmall, {color: 'red'}]}> Cancelled </Text>
        }

        return (
            <View>
                {/* tbrip card */}
                <TouchableOpacity style={styles.touchCardTrip} onPress={onPress}>
                    <View style={styles.viewTripBooked}>
                        {/* head */}
                        <View style={styles.viewHeadTripBooked}>

                            <Text style={styles.textSecondColor}>{startDate} </Text>
                            <Icon
                                name='arrow-right'
                                type='font-awesome'
                                color="#1EA896"
                                size={15} />
                            <Text style={styles.textSecondColor}> {endDate}, </Text>
                            <Text style={styles.textSecondColor}>1 Room - {numOfGuest} guests</Text>
                        </View>

                        {/* body */}
                        <View style={styles.viewBodyTripCard}>
                            <Image style={styles.imageRoom} source={{ uri: imgUri }} />
                            {/* info */}
                            <View style={styles.viewBodyInfo}>
                                {/* short info */}
                                <View>

                                    <Text style={[styles.textBlackColor, styles.textHostTitle, { width: 100 }]}
                                        numberOfLines={2} ellipsizeMode='tail'>
                                        {hostName}
                                    </Text>

                                    <View style={styles.viewAddress}>
                                        <Icon
                                            name='location-pin'
                                            type='entypo'
                                            color="#1EA896"
                                            size={14} />
                                        <Text style={[styles.textSecondColor, styles.textSmall, { width: 120 }]}
                                            numberOfLines={1} ellipsizeMode='tail' >
                                            {address}
                                        </Text>
                                    </View>

                                    <View style={{ flexDirection: 'row' }}>
                                        {this.renderStar(starVoting)}
                                    </View>

                                </View>

                                {/* price */}
                                <View style={styles.viewPrice}>
                                    <Text style={[styles.textBlackColor, styles.textPrice]}>VND {price}</Text>
                                    <Text style={[styles.textSecondColor, styles.textSmall]}>/per night</Text>

                                    {/* <Text style={[styles.textSecondColor, styles.textSmall, styles.textBookingStatus]}>
                                        Waiting host confirm
                                    </Text> */}
                                    {bookingStatus}
                                </View>

                            </View>

                        </View>
                    </View>
                </TouchableOpacity>
            </View>

        )
    }
}

const styles = StyleSheet.create({

    touchCardTrip: {
        marginTop: 5,
        marginBottom: 5 / 2,
        marginHorizontal: 10,
    },
    imageRoom: {
        width: 80,
        height: 70,
        borderRadius: 3.5
        // borderBottomLeftRadius: 5,
        // borderBottomRightRadius: 5
    },
    viewTripBooked: {
        paddingBottom: 5,
        borderRadius: 5,
        shadowColor: "rgba(0,0,0,0.75)",
        shadowOffset: {
            width: 0,
            height: 5
        },
        shadowOpacity: 0.29,
        shadowRadius: 3.84,
        elevation: 7,
        backgroundColor: '#fff',
        width: '100%'
    },
    viewHeadTripBooked: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginVertical: 10,
    },
    textSecondColor: {
        color: '#535355',
    },
    textBlackColor: {
        color: '#302f33',
    },
    viewBodyTripCard: {
        flexDirection: 'row',
        paddingLeft: 5
    },
    viewBodyInfo: {
        flexDirection: 'row',
        paddingHorizontal: 5,
        justifyContent: 'space-between',
        flex: 1,
    },
    viewAddress: {
        flexDirection: 'row',
        marginVertical: 5,
    },
    textHostTitle: {
        fontSize: 15,
        fontWeight: "700"
    },
    textSmall: {
        fontSize: 11,
    },
    textStar: {
        fontSize: 13.5,
        color: '#1EA896',
    },
    viewPrice: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    textPrice: {
        fontWeight: "700"
    },
    textBookingStatus: {
        color: 'green'
    }
});