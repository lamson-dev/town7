import { fetchApi } from "./api/api";
import deviceStorage from "./deviceStorage.service";

export async function calculateBookingPriceService(data) {
    const authToken = await deviceStorage.loadToken();
    return fetchApi("/bookings-calculate-price", "POST", data, authToken);
}