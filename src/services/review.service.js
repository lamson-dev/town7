import { fetchApi } from "./api/api";
import deviceStorage from "./deviceStorage.service";

class ReviewService {
    async review(data) {
        return await fetchApi("/host-reviews", "POST", data, await deviceStorage.loadToken());
    }
    async getById(id) {
        return await fetchApi("/host-reviews/get-by-host-id/"+id, "GET", null, await deviceStorage.loadToken());
    }1
}

export default new ReviewService();