import { put, call } from "redux-saga/effects";

import { REGISTER_USER_ERROR, REGISTER_USER_SUCCESS, AUTH_USER_ERROR, AUTH_USER_SUCCESS, LOGOUT_SUCCESS } from "../actions/types";
import { registerUserService, loginUserService, logoutUserService } from "../../services/authen.service";
import deviceStorage from "../../services/deviceStorage.service";

export function* registerSaga(payload) {
    try {

        const response = yield call(registerUserService, payload.user);

        yield put({ type: REGISTER_USER_SUCCESS, response: response.data });

        const responsLogin = yield call(loginUserService, {
            username: payload.user.username,
            password: payload.user.password
        });
        yield put({ type: AUTH_USER_SUCCESS, token: responsLogin.data.token, responsLogin: responsLogin.data.userPrinciple.user.id });

        yield call(deviceStorage.storeToken, responsLogin.data.token);
        yield call(deviceStorage.storeUserId, responsLogin.data.userPrinciple.user.id);


    } catch (error) {
        console.log('-lg error register.saga: ', error);
        yield put({ type: REGISTER_USER_ERROR, error });
    }
}

export function* loginSaga(payload) {
    try {
        const response = yield call(loginUserService, payload.user);

        console.log('-response login: ', response);

        if (!response.data.token) {
            throw new Error('Unable to find JWT in response body');
        }
        console.log('-login success');
        yield put({ type: AUTH_USER_SUCCESS, token: response.data.token, userId: response.data.userPrinciple.user.id });

        yield call(deviceStorage.storeToken, response.data.token);
        yield call(deviceStorage.storeUserId, response.data.userPrinciple.user.id);

    } catch (error) {
        console.log('-lg error auth.saga: ', error);
        yield put({ type: AUTH_USER_ERROR, error });
    }
}

export function* logoutSaga() {
    try {
        const response = yield call(logoutUserService);

        console.log('logout response: ', response);
        
        yield call(deviceStorage.removeToken);
        yield call(deviceStorage.removeUserId);


        yield put({ type: LOGOUT_SUCCESS });
    } catch (error) {
        console.log('-lg error auth.saga: ', error);
        yield put({ type: AUTH_USER_ERROR, error });
    }
}