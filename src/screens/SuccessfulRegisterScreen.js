import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Navigation } from "react-native-navigation";

import { goToLogin } from '../navigator/navigation'
import ButtonMain from "../components/ButtonMain";
import { mStyles } from "../styles/styles";

export default class SuccessfulRegisterScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    onContinuePress = () => {
        Navigation.dismissModal(this.props.componentId);
        goToLogin();
    }

    render() {
        return (
            <View style={[styles.viewMain]}>
                <View>
                    <Text style={[mStyles.textMainTitle, mStyles.mrgBottMainTitle]}>
                        Thank you for registering !
                    </Text>

                    <View style={{ marginHorizontal: 20, marginBottom: 20 }}>
                        <Text style={{ fontSize: 19.5, textAlign: 'center' }}>
                            From now on, you can suft our application to explore a lot of interesting things
                        </Text>
                    </View>
                    <View style={mStyles.viewBtnMain}>
                        <ButtonMain nameBtn={'COMEBACK LOGIN'} onPress={this.onContinuePress} />
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        justifyContent: 'center'
    },

});