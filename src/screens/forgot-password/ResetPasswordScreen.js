import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";

import TextInputComp from "../../components/TextInputComp";
import ButtonMain from "../../components/ButtonMain";
import { mStyles } from "../../styles/styles";

export default class ResetPasswordScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <View style={[styles.viewMain, mStyles.bkgColorMain]}>
                <View>
                    {/* <Text style={[mStyles.textMainTitle, mStyles.mrgBottMainTitle]}>
                        Forgot password?
                    </Text> */}

                    <View style={{ marginHorizontal: 20, marginBottom: 20 }}>
                        <Text style={{ fontWeight: 'bold' }}>
                            Please enter a new password
                        </Text>
                    </View>

                    <TextInputComp placeholder={'New Password'}
                        passwordField={true} />

                    <TextInputComp placeholder={'Re-enter New Password'}
                        passwordField={true} />

                    <View style={mStyles.viewBtnMain}>
                        <ButtonMain nameBtn={'UPDATE PASSWORD'} onPress={() => alert('hello')} />
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        justifyContent: 'center'
    },

});