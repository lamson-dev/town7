import React, { Component } from 'react';
import { ScrollView, Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { Navigation } from "react-native-navigation";

import { showAlertFeatureIncomplete } from "../utils/alert.utils";
class SideMenu extends Component {

    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);
    }

    // static options() {
    //     return {
    //         topBar: {
    //             rightButtons: [
    //                 {
    //                     id: 'closeSide',
    //                     enabled: false,
    //                     icon: require('../assets/images/icons/close-ic.png'),
    //                 },
    //             ],

    //         }
    //     };
    // }

    navigateToScreen = (route) => () => {
        showAlertFeatureIncomplete();
    }

    closeSideMenu = () => {
        Navigation.mergeOptions(this.props.componentId, {
            sideMenu: {
                left: {
                    visible: false
                }
            },
        });
        console.log('-log: closed side menu');
    }



    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View>
                        <View style={styles.navSectionStyle}>
                            <Text style={styles.navItemStyle} onPress={this.navigateToScreen(null)}>
                                Setting
                            </Text>
                        </View>
                    </View>
                </ScrollView>
                <View style={styles.footerContainer}>
                    <TouchableOpacity onPress={this.closeSideMenu}>
                        <Text>Close</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        // paddingTop: 20,
        flex: 1,
        backgroundColor: '#fff'
    },
    navItemStyle: {
        padding: 10
    },
    navSectionStyle: {
        backgroundColor: 'lightgrey'
    },
    sectionHeadingStyle: {
        backgroundColor: 'green',
        paddingVertical: 10,
        paddingHorizontal: 5
    },
    footerContainer: {
        padding: 20,
        backgroundColor: 'lightgrey'
    }
});
export default SideMenu;