import React, { Component } from "react";
import { View, StyleSheet, ScrollView, FlatList } from 'react-native';
import { Navigation } from "react-native-navigation";

import CardBookingRoom from "../../components/CardBookingRoom";
import bookingRoomService from "../../services/bookingRoom.service";
import LoaderModal from "../../components/LoaderModal";

export default class UpcomingScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bookingData: null,
            loading: false
        }
    }
    async componentDidMount() {

        this.setState({ loading: true });
        await bookingRoomService.getAll().then((response) => {
            console.log(response);
            this.setState({ loading: false });
            this.setState({ bookingData: response.data.data });
        }).catch(error => {
            this.setState({ loading: false });
            alert('Error, can not load your reservations')
        });
    }

    renderCardBookingRoom = (bookings) => {
        return (
            <FlatList
                horizontal={false}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                data={bookings}
                keyExtractor={(item, index) => item.id.toString()}
                renderItem={
                    ({ item }) =>
                        <CardBookingRoom
                            key={item.id}
                            onPress={() => this.goToBookingDetail(item.id)}
                            startDate={item.checkInDate}
                            endDate={item.checkOutDate}
                            numOfGuest={item.guests}
                            price={item.pricePerNight}
                            hostName={item.host.name}
                            address={item.host.address}
                            starVoting={item.host.stars}
                            bookingItem={item}
                            imgUri="https://vnhomestay.com.vn/img/news/homestay-la-gi_1582792415.jpg"
                        />
                }
            />
        )
    }

    goToBookingDetail = (bookingId = 0) => {

        Navigation.push('userTab', {
            component: {
                name: 'BookingDetailScreen',
                passProps: {
                    id: bookingId
                },
                options: {
                    topBar: {
                        // visible: false,

                        title: {
                            text: 'Detail Reservation'
                        }
                    },
                    bottomTabs: {
                        visible: false
                    },
                    statusBar: {
                        backgroundColor: '#1ab3c7',
                        style: 'light'
                    }
                }
            }
        });
    }

    render() {
        return (
            <View style={styles.mainView}>
                <LoaderModal
                    loading={this.state.loading} />
                <View>
                    {/* tbrip card */}
                    {this.renderCardBookingRoom(this.state.bookingData)}

                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
    },

});