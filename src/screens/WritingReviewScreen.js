import React, { Component } from "react";
import { View, Text, StyleSheet, TextInput, Image, Touc, TouchableHighlight, TouchableOpacity } from "react-native";

import ButtonMain from "../components/ButtonMain";
import ReviewService from "../services/review.service";
import { mStyles } from "../styles/styles";
import LoaderModal from "../components/LoaderModal";
import { Navigation } from "react-native-navigation";

export default class WritingReviewScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            bookingId: props.bookingId,
            starRating: 5,
            content: '',
            loading: false
        }
    }

    setStarRating = (value) => {
        this.setState({ starRating: value });
    }

    review = async () => {
        this.setState({ loading: true });
        await ReviewService.review({
            bookingId: this.state.bookingId,
            starRating: this.state.starRating,
            content: this.state.content
        }).then((response) => {
            this.setState({ loading: false });
            alert('Thanks for your rating.');
            Navigation.popToRoot(this.props.componentId);

            Navigation.mergeOptions("MainAppBottomTab", {
                bottomTabs: {
                    currentTabId: "homeTab",
                }
            });

        }).catch((error) => {
            this.setState({ loading: false });
            alert('Error: ' + error);
        })
        this.setState({ loading: false });
    }

    render() {
        return (
            <View style={[styles.viewMain, mStyles.bkgColorMain]}>
                <LoaderModal
                    loading={this.state.loading} />
                <View>
                    <Text style={[mStyles.textMainTitle, mStyles.mrgBottMainTitle]}>
                        Write a review
                    </Text>
                    <View style={styles.viewStarRating}>

                        <TouchableHighlight underlayColor="transparent" style={styles.touchStar} onPress={() => { this.setState({ starRating: 1 }); }}>
                            {this.state.starRating >= 1 ?
                                <Text style={styles.textTouchStar}>★</Text>
                                :
                                <Text style={styles.textTouchStar}>☆</Text>}
                        </TouchableHighlight>

                        <TouchableHighlight underlayColor="transparent" style={styles.touchStar} onPress={() => { this.setState({ starRating: 2 }); }}>
                            {this.state.starRating >= 2 ?
                                <Text style={styles.textTouchStar}>★</Text>
                                :
                                <Text style={styles.textTouchStar}>☆</Text>}
                        </TouchableHighlight>

                        <TouchableHighlight underlayColor="transparent" style={styles.touchStar} onPress={() => { this.setState({ starRating: 3 }); }}>
                            {this.state.starRating >= 3 ?
                                <Text style={styles.textTouchStar}>★</Text>
                                :
                                <Text style={styles.textTouchStar}>☆</Text>}
                        </TouchableHighlight>
                        <TouchableHighlight underlayColor="transparent" style={styles.touchStar} onPress={() => { this.setState({ starRating: 4 }); }}>
                            {this.state.starRating >= 4 ?
                                <Text style={styles.textTouchStar}>★</Text>
                                :
                                <Text style={styles.textTouchStar}>☆</Text>}
                        </TouchableHighlight>
                        <TouchableHighlight underlayColor="transparent" style={styles.touchStar} onPress={() => { this.setState({ starRating: 5 }); }}>
                            {this.state.starRating >= 5 ?
                                <Text style={styles.textTouchStar}>★</Text>
                                :
                                <Text style={styles.textTouchStar}>☆</Text>}
                        </TouchableHighlight>

                    </View>
                    <Text style={{ color: '#1B1B2E', textAlign: 'center' }}>Tap a star to rate</Text>

                    <View style={styles.viewInpt} >
                        <TextInput style={styles.inpTxt}
                            style={styles.textArea}
                            underlineColorAndroid="transparent"
                            placeholder="Type something"
                            placeholderTextColor="grey"
                            numberOfLines={10}
                            multiline={true}
                            onChangeText={(value) => this.setState({ content: value })}
                        />
                    </View>

                    <View style={mStyles.viewBtnMain}>
                        <ButtonMain nameBtn={'REVIEW NOW'} onPress={this.review} />
                    </View>


                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        // justifyContent: 'center'
    },

    viewAccountReady: {
        flexDirection: 'row',
        justifyContent: 'center',
    },

    viewInpt: {
        marginVertical: 5,
        flexDirection: 'row',
        marginHorizontal: 16.5,
        height: 150,
        borderRadius: 10.5,
        paddingLeft: 20,
        backgroundColor: '#FFFFFF',
    },
    inpTxt: {
        backgroundColor: 'red',
        width: "85%",
        fontSize: 16,
        textAlignVertical: "top"
    },
    viewStarRating: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    touchStar: {
        flexWrap: 'nowrap',
        // backgroundColor: 'red',
        // marginVertical: 2.5,
        // width: 26.5
    },
    textTouchStar: {
        fontSize: 45,
        color: '#FDC60A'
    }
});