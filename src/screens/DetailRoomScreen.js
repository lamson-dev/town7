import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, Dimensions } from "react-native";
import { Icon } from "react-native-elements";
import { WebView } from "react-native-webview";
import { Navigation } from "react-native-navigation";

import ButtonMain from "../components/ButtonMain";
import { mStyles } from "../styles/styles";
import { showAlertFeatureIncomplete } from "../utils/alert.utils";
import ReviewScreen from "../screens/ReviewScreen";
import CalculatePriceBookingScreen from "../screens/CalculatePriceBookingScreen";
import LoaderModal from "../components/LoaderModal";

import RoomService from "../services/room.service";

export default class DetailRoomScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            roomId: props.roomId,
            roomData: {
                hostCategory: {}
            },
            loading: false
        };
    }

    gotoReview = () => {
        Navigation.registerComponent('ReviewScreen', () => ReviewScreen)
        Navigation.push(this.props.componentId, {
            component: {
                name: 'ReviewScreen',
                passProps: {
                    hostId: this.state.roomId
                },
                options: {
                    bottomTabs: {
                        visible: false
                    },
                    topBar: {
                        backButton: {
                            color: '#3389EE',
                        },
                        // drawBehind: true,
                        background: {
                            color: '#F2F2F2'
                        },
                        noBorder: true,
                        elevation: 0,
                    }
                }
            }
        });
    }

    gotoCheckout = () => {
        Navigation.registerComponent('CalculatePriceBookingScreen', () => CalculatePriceBookingScreen)
        // Navigation.showModal({
        //     stack: {
        //         children: [
        //             {
        //                 component: {
        //                     name: 'CalculatePriceBookingScreen',
        //                     passProps: {
        //                         roomDataProps: this.state.roomData
        //                     },
        //                     options: {
        //                         bottomTabs: {
        //                             visible: false
        //                         },
        //                         topBar: {
        //                             backButton: {
        //                                 color: '#3389EE',
        //                             },
        //                             // drawBehind: true,
        //                             background: {
        //                                 color: '#F2F2F2'
        //                             },
        //                             noBorder: true,
        //                             elevation: 0,
        //                         }
        //                     }
        //                 }
        //             }
        //         ]
        //     }
        // })
        Navigation.push(this.props.componentId, {
            component: {
                name: 'CalculatePriceBookingScreen',
                passProps: {
                    roomDataProps: this.state.roomData
                },
                options: {
                    bottomTabs: {
                        visible: false
                    },
                    topBar: {
                        backButton: {
                            color: '#3389EE',
                        },
                        // drawBehind: true,
                        background: {
                            color: '#F2F2F2'
                        },
                        noBorder: true,
                        elevation: 0,
                    }
                }
            }
        });
    }

    renderStar = (numOfStar) => {
        let star = [];

        for (let index = 0; index < 5; index++) {

            if (index < numOfStar) {
                star.push(<Text key={index} style={styles.textStar}>★</Text>);
            } else {
                star.push(<Text key={index} style={styles.textStar}>☆</Text>);
            }
        };
        return star;
    }

    async componentDidMount() {
        this.setState({ loading: true });
        await RoomService.getById(this.state.roomId).then(response => {
            this.setState({
                roomData: response.data.data,
                loading: false
            });

            console.log('data room detail: ', response);
        }).catch(error => {
            this.setState({ loading: false });
            alert('Error: ' + error);
        })
        this.setState({ loading: false });
    }

    render() {

        return (
            <View style={[styles.viewMain, mStyles.bkgColorMain]}>
                <LoaderModal
                    loading={this.state.loading} />
                {/* top view: image background, name of room */}
                <View style={{ backgroundColor: '#ffff' }}>
                    {/* Image */}
                    <View style={{ flexDirection: 'row' }}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            <Image style={styles.imageRoom} source={{ uri: "https://ezcloud.vn/wp-content/uploads/2019/03/kinh-doanh-homestay-kiem-tien-trieu.jpg" }} />
                            <Image style={styles.imageRoom} source={{ uri: "https://danang24.vn/Images/TV/LittleM26.jpg" }} />
                        </ScrollView>
                    </View>
                    {/* Wish Button */}
                    <View style={styles.viewWish_Share}>
                        {/* Wish button */}
                        <TouchableOpacity
                            style={styles.touchWish_Share}
                            onPress={() => showAlertFeatureIncomplete()}>
                            <Icon
                                name='heart-outlined'
                                // name='heart'
                                type='entypo'
                                color="white"
                                size={30} />
                        </TouchableOpacity>

                        {/* Share Button */}
                        <TouchableOpacity
                            style={styles.touchWish_Share}
                            onPress={() => showAlertFeatureIncomplete()}>
                            <Icon
                                name='share-alternative'
                                type='entypo'
                                color="white"
                                size={26.5} />
                        </TouchableOpacity>
                    </View>

                </View>

                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.viewBody}>


                        {/* Price and Type of room */}
                        <View style={styles.viewPrice_Roomtype}>
                            <Text style={styles.textPrice}>
                                VND {this.state.roomData.standardPriceMondayToThursday}/night
                            </Text>
                            <Text style={styles.textTypeRoom}>
                                {this.state.roomData['hostCategory'].name}
                            </Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Icon
                                type='font-awesome'
                                name='users'
                                color="#546375"
                                size={12.5} />
                            <Text>
                                {this.state.roomData.numberOfMaximumGuest} guests
                            </Text>
                        </View>

                        {/* Name of room */}
                        <Text style={styles.textNameRoom}>
                            {this.state.roomData.name}
                        </Text>

                        {/* Stars and reviews*/}
                        <View style={styles.viewStar_Review}>
                            <View style={{ flexDirection: 'row' }}>
                                {this.renderStar(this.state.roomData.star)}
                            </View>

                            <TouchableOpacity onPress={() => this.gotoReview()}>
                                <View style={{ flexDirection: 'row', }}>
                                    <Text style={{ color: '#4A90E2' }}>
                                        {this.state.roomData.totalReview ? this.state.roomData.totalReview : 0} reviews
                                    </Text>
                                    <Icon
                                        style={styles.iconRight}
                                        name='right'
                                        type='antdesign'
                                        color="#3389EE"
                                        size={17.5}
                                    />
                                </View>
                            </TouchableOpacity>
                        </View>

                        {/* Services */}
                        <View style={styles.viewServices}>

                            <View style={styles.viewServiceSub}>
                                <Icon
                                    style={styles.iconRight}
                                    name='wifi'
                                    type='materialIcons'
                                    color="#546375"
                                    size={17.5}
                                />
                                <Text>Free Wifi</Text>
                            </View>

                            <View style={styles.viewServiceSub}>
                                <Icon
                                    style={styles.iconRight}
                                    name='free-breakfast'
                                    type='materialIcons'
                                    color="#546375"
                                    size={17.5}
                                />
                                <Text>Breakfast</Text>
                            </View>

                            <View style={styles.viewServiceSub}>
                                <Icon
                                    style={styles.iconRight}
                                    name='pool'
                                    type='materialIcons'
                                    color="#546375"
                                    size={17.5}
                                />
                                <Text>Pool</Text>
                            </View>

                            <View style={styles.viewServiceSub}>
                                <Icon
                                    style={styles.iconRight}
                                    name='pets'
                                    type='materialIcons'
                                    color="#546375"
                                    size={17.5}
                                />
                                <Text>Pets</Text>
                            </View>

                        </View>

                        {/* Desciptions */}
                        <View style={styles.viewDescription}>
                            <Text style={styles.textDescription} numberOfLines={4}>
                                {this.state.roomData.description}
                            </Text>
                        </View>

                        {/* Location address and map*/}
                        <View style={styles.viewAddress_Map}>
                            <View style={styles.viewAddressText}>
                                <Icon
                                    style={styles.iconRight}
                                    name='location-on'
                                    type='materialIcons'
                                    color="#546375"
                                    size={17.5}
                                />
                                <Text style={styles.textAddress}>
                                    {this.state.roomData.address}
                                </Text>
                            </View>
                            {/* map view */}
                            <View style={{ height: 200, width: "100%" }}>
                                <WebView
                                    source={{ uri: "http://townhouse-cloud.dx.am/?location=" + this.state.roomData.address }} />
                            </View>
                        </View>

                    </View>
                </ScrollView>

                {/* Book room button */}
                <View style={styles.viewButtonBook}>
                    <ButtonMain nameBtn={'BOOK NOW'} onPress={this.gotoCheckout} />
                </View>
            </View>
        )
    }
}

const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
    },

    imageRoom: {
        width: windowWidth,
        height: 213,
        marginRight: 1.5
    },
    viewWish_Share: {
        flexDirection: 'row',
        // backgroundColor: 'grey',
        position: 'absolute',
        right: 5,
        top: 15
    },
    touchWish_Share: {
        marginHorizontal: 10
    },

    viewPrice_Roomtype: {
        flexDirection: 'row',
        marginTop: 17,
        paddingBottom: 10,
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: '#E0E0E1'
    },
    textPrice: {
        fontSize: 20,
        fontStyle: 'normal',
        fontWeight: 'bold',
        lineHeight: 24,
        color: '#ff8519',
        letterSpacing: 2,

    },
    textTypeRoom: {
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: 'bold',
        lineHeight: 14,
        color: '#c4194a',
        letterSpacing: 2,
        textTransform: 'uppercase',
        textShadowColor: 'rgba(166, 29, 85, 0.2)',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10,
    },

    textNameRoom: {
        width: 335,
        fontSize: 23,
        fontStyle: 'normal',
        fontWeight: 'bold',
        color: '#293340',

    },

    viewBody: {
        marginHorizontal: 20,
    },

    viewStar_Review: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: '#E0E0E1'
    },
    viewRatingStar: {
        flexDirection: 'row',
    },
    textStar: {
        fontSize: 16.5,
        color: 'orange',
    },

    viewServices: {
        flexDirection: 'row',
        paddingVertical: 11,
        borderBottomWidth: 1,
        borderBottomColor: '#E0E0E1'
    },
    viewServiceSub: {
        marginHorizontal: 10
    },

    viewDescription: {
        paddingTop: 5.5,
    },
    textDescription: {
        fontStyle: "normal",
        fontWeight: "500",
        fontSize: 15,
        lineHeight: 22,
        color: '#39393A'
    },

    viewAddress_Map: {
        paddingTop: 5.5,
    },
    viewAddressText: {
        flexDirection: 'row',
        marginBottom: 3
    },
    textAddress: {
        fontStyle: "normal",
        fontWeight: 'bold'
    },

    addToCartTouchView: {
        height: 50,
        marginTop: 15,
        backgroundColor: '#fc9619',
    },
    touchAddToCart: {
        flex: 1,
    },
    textAddToCartTouch: {
        color: 'white',
        marginVertical: 13.5,
        textAlign: 'center',
        fontWeight: '600',
        fontSize: 16.5
    },

    viewButtonBook: {
        paddingBottom: 7,
        marginHorizontal: 19,
    }
});