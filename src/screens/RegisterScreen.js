import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity, ScrollView } from "react-native";

import { Provider } from "react-redux";

import RegisterForm from "../forms/RegisterForm";
import { mStyles } from "../styles/styles";
import store from "../redux/store/store";
import { Navigation } from "react-native-navigation";

export default class RegisterScreen extends Component {

    popLogin = () => {
        Navigation.pop(this.props.componentId);
    }

    showSuccessfulRegisterModal = () => {
        Navigation.showModal({
            stack: {
                children: [
                    {
                        component: {
                            name: "SuccessfulRegisterScreen"
                        }
                    }
                ]
            }
        })
    }

    register = (registerStatus) => {
        console.log('register scr: ', registerStatus);
        if (registerStatus === false) {
            alert('Register new account is failed. Please check your information');
        }

        if (registerStatus) {
            console.log('register Success');
            Navigation.pop(this.props.componentId);
            this.showSuccessfulRegisterModal();
        }
    }

    render() {
        return (
            <View style={[styles.viewMain, mStyles.bkgColorMain]}>
                <ScrollView>
                    <Text style={[mStyles.textMainTitle, mStyles.mrgBottMainTitle]}>
                        Sign Up
                    </Text>

                    <Provider store={store}>
                        <RegisterForm registerProps={this.register} />
                    </Provider>

                    <View style={styles.viewAccountReady}>
                        <Text style={{ textAlign: 'center', marginRight: 5 }}>
                            Already have an account ?
                        </Text>
                        <TouchableOpacity onPress={this.popLogin}>
                            <Text style={{ color: '#3389EE', fontWeight: 'bold' }}>
                                Sign in
                            </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        justifyContent: 'center'
    },

    viewSignUpBtn: {
        marginHorizontal: 20,
        // marginTop: 10,
        marginBottom: 25
    },
    viewAccountReady: {
        flexDirection: 'row',
        justifyContent: 'center',
    }
});