import * as React from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';

import UpcomingScreen from "./booking-tab/UpcomingScreen";
import CompletedScreen from "./booking-tab/CompletedScreen";
import CancelledScreen from "./booking-tab/CancelledScreen";

const initialLayout = { width: Dimensions.get('window').width };

export default function MyBookingScreen() {

  const [routes] = React.useState([
    { key: 'upcoming_tab', title: 'ALL' },
    { key: 'completed_tab', title: 'COMPLETED' },
    { key: 'cancelled_tab', title: 'CANCELLED' }
  ]);

  const [index, setIndex] = React.useState(0);

  const renderScene = SceneMap({
    upcoming_tab: UpcomingScreen,
    completed_tab: CompletedScreen,
    cancelled_tab: CancelledScreen,
  });



  const renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={{ backgroundColor: '#24c3f0', height: 5, borderRadius: 1 }}
      style={{ backgroundColor: '#fff', border: 'none', marginHorizontal: 5 }}
      activeColor="#24c3f0"
      // activeColor={}
      // tabStyle={{backgroundColor:'blue'}}
      inactiveColor="#b1b0b5"
    // renderLabel={({ route, color }) => (
    //   <Text style={{ color, margin: 8 }}>
    //     {route.title}-lb
    //   </Text>
    // )}
    />
  );

  return (
    <TabView
      renderTabBar={renderTabBar}
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
    />
  );
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
});