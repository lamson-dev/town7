import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, FlatList, Keyboard } from "react-native";
import { Navigation } from "react-native-navigation";

import TextInputComp from "../components/TextInputComp";
import { CardType1, CardType2, CardType3 } from "../components/CardHost";
import { fetchApi } from "../services/api/api";
import deviceStorage from "../services/deviceStorage.service";

import DetailRoomScreen from "./DetailRoomScreen";
import SearchListViewScreen from "./SearchListViewScreen";
import { showAlertFeatureIncomplete } from "../utils/alert.utils";
import RoomService from "../services/room.service";

export default class HomeScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            hostRecommendedData: [],
            popularDesData: [],
            hostAroundWorld: [],
            searchKeywords: ''
        };
    }

    gotoRoomDetail = (roomId) => {
        Navigation.registerComponent('DetailRoomScreen', () => DetailRoomScreen)
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        id: "RoomDetailModal",
                        name: 'DetailRoomScreen',
                        passProps: {
                            roomId: roomId
                        },
                        options: {
                            bottomTabs: {
                                visible: false
                            },
                            topBar: {
                                backButton: {
                                    color: '#ffff',
                                },
                                drawBehind: true,
                                background: {
                                    color: 'transparent'
                                },
                                noBorder: true,
                                elevation: 0,
                            }
                        }
                    }
                }]
            }
        });
    }

    gotoSearchResultList = () => {
        Navigation.registerComponent('SearchListViewScreen', () => SearchListViewScreen)
        Navigation.push(this.props.componentId, {
            component: {
                name: 'SearchListViewScreen',
                passProps: {
                    keywordProps: this.state.searchKeywords
                },
                options: {
                    bottomTabs: {
                        visible: false
                    },
                    topBar: {
                        backButton: {
                            color: '#ffff',
                        },
                        drawBehind: true,
                        background: {
                            color: 'transparent'
                        },
                        noBorder: true,
                        elevation: 0,
                    }
                }
            }
        });
    }

    gotoListView(typeView = 'all', title = '', cityId = 0,) {
        Navigation.push(this.props.componentId, {
            component: {
                name: 'ListingViewScreen',
                passProps: {
                    typeView: typeView,
                    cityId: cityId
                },
                options: {
                    bottomTabs: {
                        visible: false
                    },
                    topBar: {
                        title: {
                            text: title
                        }
                    }
                }
            }
        });
    }



    searchDestinationHost = async () => {
        Keyboard.dismiss();
        this.gotoSearchResultList();
    }

    // Recommended homestays for you
    renderCardType1 = (host) => {
        return (
            <FlatList
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                data={host}
                keyExtractor={(item, index) => item.id.toString()}
                renderItem={
                    ({ item }) =>
                        <CardType1
                            onPress={() => this.gotoRoomDetail(item.id)}
                            hostName={item.name}
                            imgUri={"https://ezcloud.vn/wp-content/uploads/2019/03/kinh-doanh-homestay-kiem-tien-trieu.jpg"}
                            price={item.standardPriceMondayToThursday}
                            starVoting={item.stars ? Math.round(item.stars * 10) / 10 : 0}
                            reviewTotal={item.totalReview ? item.totalReview : 0} />
                }
            />
        )
    }

    // Homestay around the world
    renderCardType2 = (host) => {
        return (
            <FlatList
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                data={host}
                keyExtractor={(item, index) => item.id.toString()}
                renderItem={
                    ({ item }) =>
                        <CardType2
                            onPress={() => this.gotoRoomDetail(item.id)}
                            hostName={item.name}
                            imgUri={"https://danang24.vn/Images/TV/LittleM26.jpg"}
                            price={item.standardPriceMondayToThursday}
                            starVoting={item.stars ? Math.round(item.stars * 10) / 10 : 0}
                            totalReview={item.totalReview ? item.totalReview : 0} />
                }
            />
        )
    }

    // Popular destination
    renderCardType3 = (destination) => {
        return (
            <FlatList
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                data={destination}
                keyExtractor={(item, index) => item.id.toString()}
                renderItem={
                    ({ item }) =>
                        <CardType3
                            onPress={() => this.gotoListView('city', item.name, item.id)}
                            imgUri={"https://danangreview.com/wp-content/uploads/2018/09/hoi-an-ve-ban-dem.jpg"}
                            destination={item.name} />
                }
            />
        )
    }

    async componentDidMount() {
        const loginToken = await deviceStorage.loadToken();
        const dataCities = await fetchApi('/host-cities', 'GET', null, loginToken);

        this.setState({
            popularDesData: dataCities.data.data,
        });

        await RoomService.getByRecommendation().then(response => {
            this.setState({
                hostRecommendedData: response.data.data,
            });
        });

        await RoomService.getAll().then(response => {
            this.setState({
                hostAroundWorld: response.data.data,
            })
        });

    }

    render() {
        return (
            <View style={[styles.viewMain]}>
                {/* <StatusBar hidden={false} backgroundColor="#24c3f0" barStyle="light-content" translucent={true} /> */}
                {/* search view */}
                <View style={styles.viewTop}>
                    <View style={styles.viewLogo}>
                        <Image style={styles.imgLogo} source={require('../assets/images/Logo-app.png')}></Image>
                    </View>
                    <TextInputComp
                        placeholder={'Where to?'}
                        buttonRight={true}
                        nameIconBtn={'search'}
                        typeIconBtn={'fontawesome5'}
                        onChangeText={(value) => this.setState({ searchKeywords: value })}
                        buttonRightPress={() => this.searchDestinationHost()}
                        onSubmitEditing={() => this.searchDestinationHost()} />
                </View>

                <ScrollView>


                    {/* recommended homestays for you */}
                    <View style={styles.viewTab}>
                        <View style={styles.viewTitleTab}>
                            <Text style={styles.textTitleTab}>
                                RECOMMENDED STAY FOR YOU
                        </Text>
                        </View>

                        {/* List Card */}
                        <View style={styles.viewListCard}>

                            <View style={styles.viewRowListCard}>
                                {/* Homestay card */}
                                {this.renderCardType1(this.state.hostRecommendedData)}
                            </View>
                        </View>
                    </View>

                    {/* Popular destination */}
                    <View style={styles.viewTab}>
                        <View style={styles.viewTitleTab}>
                            <Text style={styles.textTitleTab}>
                                POPULAR DESTINATION
                        </Text>
                        </View>

                        {/* List Card */}
                        <View style={styles.viewListCard}>

                            <View style={styles.viewRowListCard}>
                                {/* Homestay card */}
                                {this.renderCardType3(this.state.popularDesData)}
                            </View>
                        </View>
                    </View>

                    {/* Homestay around the world */}
                    <View style={styles.viewTab}>
                        <View style={styles.viewTitleTab}>
                            <Text style={styles.textTitleTab}>
                                HOME STAY AROUND THE WORLD
                            </Text>
                        </View>

                        {/* List Card */}
                        <View style={styles.viewListCard}>

                            <View style={styles.viewRowListCard}>
                                {/* Homestay card */}
                                {this.renderCardType2(this.state.hostAroundWorld)}
                            </View>

                            {/* See more button */}
                            <View style={styles.viewSeeMore}>
                                <TouchableOpacity style={styles.touchSeeMore}>
                                    <Text style={styles.textSeeMore} onPress={() => this.gotoListView('all','All Hosts')}>
                                        See More +
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        // justifyContent: 'center'
    },
    viewTop: {
        width: "100%",
        paddingBottom: 2.5,
        marginBottom: 3.5,
        backgroundColor: '#24c3f0',

        shadowColor: "black",
        shadowOffset: {
            width: 0,
            height: 0.5 * 5
        },
        shadowOpacity: 0.3,
        shadowRadius: 0.8 * 5,
        elevation: 5,
    },
    viewLogo: {
        marginVertical: 5
    },
    imgLogo: {
        width: 85,
        height: 40,
        alignSelf: 'center',
    },

    viewTab: {
        backgroundColor: "#ffffff",
        paddingLeft: 20,
        marginTop: 15
    },
    viewTitleTab: {
        marginBottom: 16,
        marginTop: 16
    },
    textTitleTab: {
        color: '#293340',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 15.5,
        letterSpacing: 0.7,
        textTransform: 'uppercase'
    },

    viewListCard: {
        // backgroundColor: '#d5dceb'
    },
    viewRowListCard: {
        flexDirection: 'row'
    },


    viewSeeMore: {
        // backgroundColor: "#3389EE",
        height: 35,
        width: "80%",
        marginTop: 5,
        marginLeft: "auto",
        marginRight: "auto",
    },
    touchSeeMore: {
        flex: 1,
        borderRadius: 15,
        borderWidth: 1.75,
        borderColor: '#3389EE',
        justifyContent: 'center'
    },
    textSeeMore: {
        textAlign: 'center'
    }
});