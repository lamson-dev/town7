import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Navigation } from "react-native-navigation";

import ButtonMain from "../components/ButtonMain";
import { mStyles } from "../styles/styles";

export default class SuccessfulBookingScreen extends Component {

    onContinuePress = () => {

        Navigation.dismissAllModals();

        Navigation.mergeOptions("MainAppBottomTab", {
            bottomTabs: {
                currentTabId: "userTab",
            }
        });

        Navigation.push("userTab", {
            component: {
                name: 'MyReservationScreen',
                passProps: {
                    userDataProps: this.props.userDataProps
                },
                options: {
                    topBar: {
                        title: {
                            text: 'My Reservation'
                        }
                    },
                    bottomTabs: {
                        visible: false
                    }
                }
            }
        });
    }

    render() {
        return (
            <View style={[styles.viewMain]}>
                <View>
                    <Text style={[mStyles.textMainTitle, mStyles.mrgBottMainTitle]}>
                        Your reservation has been request successfully !
                    </Text>

                    <View style={{ marginHorizontal: 20, marginBottom: 20 }}>
                        <Text style={{ fontSize: 19.5, textAlign: 'center' }}>
                            Please. Waiting for our partner confirm your reservation
                        </Text>
                    </View>
                    <View style={mStyles.viewBtnMain}>
                        <ButtonMain nameBtn={'MY RESERVATIONS'} onPress={this.onContinuePress} />
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        justifyContent: 'center'
    },

});