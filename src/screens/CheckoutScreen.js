import React, { Component } from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";
import { Navigation } from "react-native-navigation";

import ButtonMain from "../components/ButtonMain";
import { mStyles } from "../styles/styles";
import LoaderModal from "../components/LoaderModal";
import bookingRoomService from "../services/bookingRoom.service";

export default class CheckoutScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            bookingData: props.bookingDataProps,
            loading: false
        }
    }

    bookingRoom = async () => {


        this.setState({ loading: true });
        await bookingRoomService.booking(this.state.bookingData).then(() => {
            this.setState({ loading: false });

            Navigation.showModal({
                stack: {
                    children: [
                        {
                            component: {
                                name: "SuccessfulBookingScreen"
                            }
                        }
                    ]
                }
            });
            Navigation.dismissModal("RoomDetailModal");

        }).catch(error => {
            this.setState({ loading: false });
            alert('Error: can not booking ' + error);
        });
        this.setState({ loading: false });
    }
    render() {
        return (
            <View style={[styles.viewMain, mStyles.bkgColorMain]}>
                <LoaderModal
                    loading={this.state.loading} />
                <ScrollView>
                    {/* <View>
                        <Text>Check Out</Text>
                    </View> */}

                    <View style={styles.viewCheckInOutRoom}>

                        <View style={styles.viewItemInfo}>
                            <Text style={styles.textTouchLabel}>Check-In</Text>
                            <Text style={styles.textTouchProperty}>
                                {this.state.bookingData.startDate}
                            </Text>
                        </View>

                        <View style={styles.viewItemInfo}>
                            <Text style={styles.textTouchLabel}>Check-Out</Text>
                            <Text style={styles.textTouchProperty}>
                                {this.state.bookingData.endDate}
                            </Text>
                        </View>

                    </View>

                    <View style={styles.viewCheckInOutRoom}>

                        <View style={styles.viewItemInfo}>
                            <Text style={styles.textTouchLabel}>Guests</Text>
                            <Text style={styles.textTouchProperty}>
                                {this.state.bookingData.guests}
                            </Text>
                        </View>

                        <View style={styles.viewItemInfo}>
                            <Text style={styles.textTouchLabel}>Staying</Text>
                            <Text style={styles.textTouchProperty}>
                                {this.state.bookingData.nights} Nights
                            </Text>
                        </View>

                    </View>

                    <View style={styles.viewFee}>
                        <Text style={styles.textFeeTax}>
                            PAYMENT DETAIL
                        </Text>

                        <View style={styles.viewFeeTax}>

                            <View style={styles.viewFeeTaxSub}>
                                <Text>
                                    One room x {this.state.bookingData.nights} nights
                        </Text>
                                <Text>
                                    VND {(this.state.bookingData.nights * this.state.bookingData.pricePerNight)}
                                </Text>
                            </View>

                            <View style={styles.viewFeeTaxSub}>
                                <Text>
                                    Cleaning fee
                            </Text>
                                <Text>
                                    VND 0
                            </Text>
                            </View>

                            <View style={styles.viewFeeTaxSub}>
                                <Text>
                                    Service fee
                            </Text>
                                <Text>
                                    VND {this.state.bookingData.serviceCharge}
                                </Text>
                            </View>

                        </View>
                    </View>

                    {/* Total fee */}
                    <View style={styles.viewTotalFee}>
                        <Text style={styles.textTotalFee}>
                            Total
                    </Text>
                        <Text style={[styles.textTotalFee, { fontWeight: 'bold', color: 'red' }]}>
                            VND {this.state.bookingData.totalPrice}
                        </Text>
                    </View>
                </ScrollView>

                <View style={styles.viewButtonBook}>
                    <ButtonMain
                        nameBtn={'BOOKING'}
                        onPress={this.bookingRoom} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
    },
    viewRoomInfo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 20,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#fff'
    },
    textRoomType: {
        fontSize: 14.5,
        fontStyle: 'normal',
        fontWeight: 'bold',
        color: '#c4194a',
        letterSpacing: 2,
        textTransform: 'uppercase'
    },
    textPrice: {
        fontSize: 20,
        fontStyle: 'normal',
        lineHeight: 24,
        color: '#282a2e',
        marginTop: 10
    },
    imageRoom: {
        width: 110,
        height: 80,
    },

    viewCheckInOutRoom: {
        flexDirection: 'row',
        marginTop: 20,
        marginHorizontal: 20,
        marginBottom: 15,
    },
    viewItemInfo: {
        // backgroundColor: 'red',
        flex: 1
    },
    touchProperty: {
        marginBottom: 5,
        width: 100,
        height: 60,
        justifyContent: 'center',
        borderWidth: 0.7,
        borderRadius: 8,
        borderColor: '#fff',
        backgroundColor: '#FFFF',
    },
    textTouchLabel: {
        fontWeight: 'bold',
        fontSize: 10.5,
        textAlign: "left",
        textTransform: 'uppercase',
        color: '#91929e',
        marginBottom: 2
    },
    textTouchProperty: {
        textAlign: 'left',
        fontWeight: '200',
        fontSize: 17,
        lineHeight: 20,
        color: '#1e1e1f',
    },

    viewFee: {
        // backgroundColor: 'blue',
        marginTop: 10,
        marginHorizontal: 20,

        paddingTop: 15,
        borderTopWidth: 1,
        borderTopColor: '#fff',

        paddingBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#fff'

    },
    textFeeTax: {
        fontWeight: 'bold',
        color: '#1e1e1f',
        fontSize: 13
    },
    viewFeeTax: {
        marginVertical: 5
    },
    viewFeeTaxSub: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        // backgroundColor: 'blue',
        marginVertical: 5
    },

    viewTotalFee: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        marginTop: 5
    },
    textTotalFee: {
        fontSize: 14.5,
    },
    viewButtonBook: {
        paddingBottom: 7,
        marginHorizontal: 19,
    }
});