import { Navigation } from "react-native-navigation";

// import IntroScreen from "../screens/IntroScreen";
import LoginScreen from "../screens/LoginScreen";
import RegisterScreen from "../screens/RegisterScreen";
import SuccessfulRegisterScreen from "../screens/SuccessfulRegisterScreen";
import ForgotPasswordScreen from "../screens/ForgotPasswordScreen";
import VerificationCodeScreen from "../screens/forgot-password/VerificationCodeScreen";
import ResetPasswordScreen from "../screens/forgot-password/ResetPasswordScreen";
import SuccessfulResetPasswordScreen from "../screens/forgot-password/SuccessfulResetPasswordScreen";
import HomeScreen from "../screens/HomeScreen";
import UserProfileScreen from "../screens/UserProfileScreen";
import NotificationScreen from "../screens/NotificationScreen";
import ReviewScreen from "../screens/ReviewScreen";
import WritingReviewScreen from "../screens/WritingReviewScreen";
import PaymentScreen from "../screens/PaymentScreen";
import SuccessfulPaymentScreen from "../screens/SuccessfulPaymentScreen";
import SuccessfulBookingScreen from "../screens/SuccessfulBookingScreen";
import ListingViewScreen from "../screens/ListingViewScreen";
import MyBookingScreen from "../screens/MyBookingScreen";
import MyReservationScreen from "../screens/MyReservationScreen";
import BookingDetailScreen from "../screens/booking-tab/BookingDetailScreen";
import UpcomingScreen from "../screens/booking-tab/UpcomingScreen";
import SideMenu from "../screens/SideMenu";

export function registerScreen() {
  Navigation.registerComponent('LoginScreen', () => LoginScreen);
  Navigation.registerComponent('RegisterScreen', () => RegisterScreen);
  Navigation.registerComponent('SuccessfulRegisterScreen', () => SuccessfulRegisterScreen);
  Navigation.registerComponent('ForgotPasswordScreen', () => ForgotPasswordScreen);
  Navigation.registerComponent('VerificationCodeScreen', () => VerificationCodeScreen);
  Navigation.registerComponent('ResetPasswordScreen', () => ResetPasswordScreen);
  Navigation.registerComponent('SuccessfulResetPasswordScreen', () => SuccessfulResetPasswordScreen);
  Navigation.registerComponent('HomeScreen', () => HomeScreen);
  Navigation.registerComponent('UserProfileScreen', () => UserProfileScreen);
  Navigation.registerComponent('NotificationScreen', () => NotificationScreen);
  Navigation.registerComponent('PaymentScreen', () => PaymentScreen);
  Navigation.registerComponent('ReviewScreen', () => ReviewScreen);
  Navigation.registerComponent('ReviewScreen', () => ReviewScreen);
  Navigation.registerComponent('SuccessfulPaymentScreen', () => SuccessfulPaymentScreen);
  Navigation.registerComponent('SuccessfulBookingScreen', () => SuccessfulBookingScreen);
  Navigation.registerComponent('WritingReviewScreen', () => WritingReviewScreen);
  Navigation.registerComponent('ListingViewScreen', () => ListingViewScreen);
  Navigation.registerComponent('MyBookingScreen', () => MyBookingScreen);
  Navigation.registerComponent('MyReservationScreen', () => MyReservationScreen);
  Navigation.registerComponent('BookingDetailScreen', () => BookingDetailScreen);
  Navigation.registerComponent('UpcomingScreen', () => UpcomingScreen);
  Navigation.registerComponent('SideMenu', () => SideMenu);
}